package com.luv2code.tests.srp;

import com.luv2code.pages.main.GooglePage;
import com.luv2code.pages.result.GoogleResultPage;
import com.luv2code.tests.BaseTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class GoogleTest extends BaseTest {

    private GooglePage googlePage;
    private GoogleResultPage googleResultPage;

    @Test(dataProvider = "googleSearch")
    public void searchGoogle(String keyword, int index, String linkName) {
        this.googlePage = new GooglePage(driver);
        this.googleResultPage = new GoogleResultPage(driver);

        googlePage.goTo();
        googlePage.getSearchComponent().enter(keyword);
        googlePage.getSearchSuggestionComponent().clickItem(index);
        googleResultPage.getSearchComponent().enter(keyword);
        googleResultPage.getSearchSuggestionComponent().clickItem(index);
        googleResultPage.getNavigationBarComponent().gotoBarLink(linkName);
        System.out.println(googleResultPage.getResultStatComponent().getStat());
    }

    @DataProvider(name = "googleSearch")
    private Object[][] getData() {
        return new Object[][] {
                {"Single Principle", 5, "Video"}
        };
    }

}
