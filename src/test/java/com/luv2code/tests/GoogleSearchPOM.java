package com.luv2code.tests;

import com.luv2code.pages.oldPageObjectsModel.GoogleMainPage;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class GoogleSearchPOM extends BaseTest{
    private GoogleMainPage googleMainPage;

    @Test(dataProvider = "googleSearch")
    public void searchGoogle(String keyword, int index, String linkName) {
        googleMainPage = new GoogleMainPage(driver);
        googleMainPage.goTo();
        googleMainPage.enter(keyword);
        googleMainPage.clickItem(index);
        googleMainPage.enter(keyword);
        googleMainPage.clickItem(index);
        googleMainPage.gotoBarLink(linkName);
        System.out.println(googleMainPage.getStat());
    }

    @DataProvider(name = "googleSearch")
    private Object[][] getData() {
        return new Object[][] {
                {"Single Principle", 5, "Video"}
        };
    }

}
