
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class GoogleTestFlow {

    @Test
    public void printSearchResult() throws InterruptedException {

        // Launch Google site
        WebDriver driver = new FirefoxDriver();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://google.com");

        // Enter the given keyword on the search text-box
        driver.findElement(By.name("q")).sendKeys("single principle");
        Thread.sleep(1000);
        // Click on the n suggestion
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//li[4]//div[@class='sbtc']"))));
        driver.findElement(By.xpath("//li[4]//div[@class='sbtc']")).click();
        driver.findElement(By.cssSelector("span.lBbtTb > [focusable='false']")).click();


        // Enter the given keyword on the Google Result Page
        driver.findElement(By.name("q")).sendKeys("selena");

        // Click on the n suggestion again
        Thread.sleep(1000);

        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//span[.='selena gomez']"))));
        driver.findElement(By.xpath("//span[.='selena gomez']")).click();
        // Go to News/Videos
        driver.findElement(By.cssSelector("div#hdtb-msb-vis > div:nth-of-type(3) > .q")).click();
        // Print the search result
        WebElement result = driver.findElement(By.id("result-stats"));
        System.out.println(result.getText());

        driver.quit();




    }
}
