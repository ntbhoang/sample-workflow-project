package com.luv2code.pages.main;

import com.google.common.util.concurrent.Uninterruptibles;
import com.luv2code.pages.common.AbstractComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

public class SearchComponent extends AbstractComponent {

    @FindBy(name = "q")
    private WebElement searchTextBox;

    public SearchComponent(WebDriver driver) {
        super(driver);
    }

    public void enter(String keyword) {
        this.searchTextBox.clear();
        for (char ch : keyword.toCharArray()) {
            Uninterruptibles.sleepUninterruptibly(50, TimeUnit.MILLISECONDS);
            this.searchTextBox.sendKeys(ch + "");
        }
    }



    @Override
    public boolean isDisplayed() {
       //return this.wait.until(
               // ExpectedConditions.elementToBeClickable(searchTextBox)).isDisplayed();
        return this.wait.until((ele) -> searchTextBox.isDisplayed());


       // Function<WebElement, Boolean> isDisplayed = (element) -> element.isDisplayed();
    }
}
