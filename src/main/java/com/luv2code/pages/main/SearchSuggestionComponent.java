package com.luv2code.pages.main;

import com.luv2code.pages.common.AbstractComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchSuggestionComponent extends AbstractComponent {

    @FindBy(css = "li.sbct")
    private List<WebElement> suggestions;

    public SearchSuggestionComponent(WebDriver driver) {
        super(driver);
    }

    public void clickItem(int index) {
        this.suggestions.get(index - 1).click();
    }

    @Override
    public boolean isDisplayed() {
        return this.wait.until((ele) -> suggestions.size() > 3);
    }

    // Funcional Programming in Automation
}
