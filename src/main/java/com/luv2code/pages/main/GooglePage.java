package com.luv2code.pages.main;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class GooglePage {

    private WebDriver driver;
    private SearchComponent searchComponent;
    private SearchSuggestionComponent searchSuggestionComponent;

    public GooglePage(final WebDriver driver) {
        this.driver = driver;
        this.searchComponent = PageFactory.initElements(driver, SearchComponent.class);
        this.searchSuggestionComponent = PageFactory.initElements(driver, SearchSuggestionComponent.class);
    }

    public void goTo() {
        this.driver.get("https://www.google.com");
    }

    public SearchComponent getSearchComponent() {
        return this.searchComponent;
    }

    public SearchSuggestionComponent getSearchSuggestionComponent() {
        return this.searchSuggestionComponent;
    }

}
