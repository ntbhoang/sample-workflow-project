package com.luv2code.pages.result;

import com.luv2code.pages.common.AbstractComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ResultStatComponent extends AbstractComponent {

    @FindBy(id = "result-stats")
    private WebElement resultStat;

    public ResultStatComponent(WebDriver driver) {
        super(driver);
    }

    public String getStat() {
        return this.resultStat.getText();
    }

    @Override
    public boolean isDisplayed() {
        return this.wait.until((ele) -> this.resultStat.isDisplayed());
    }
}
