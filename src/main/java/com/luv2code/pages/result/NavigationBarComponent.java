package com.luv2code.pages.result;

import com.luv2code.pages.common.AbstractComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NavigationBarComponent extends AbstractComponent {

    @FindBy(id = "hdtb")
    private WebElement bar;

    @FindBy(linkText = "Images")
    private WebElement imagesLink;

    @FindBy(linkText = "News")
    private WebElement newsLink;

    @FindBy(linkText = "Video")
    private WebElement videoLink;


    public NavigationBarComponent(WebDriver driver) {
        super(driver);
    }

    private void gotoImages() {
        this.imagesLink.click();
    }

    private void gotoNews() {
        this.newsLink.click();
    }

    private void gotoVideo() {
        this.videoLink.click();
    }

    public void gotoBarLink(String linkName) {
        if (linkName.toLowerCase().equals("Images")) {
            gotoImages();
        } else if (linkName.toLowerCase().equals("News")) {
            gotoNews();
        } else {
            gotoVideo();
        }
    }

    @Override
    public boolean isDisplayed() {
        return this.wait.until((ele) -> this.bar.isDisplayed());
    }
}
