package com.luv2code.pages.result;

import com.luv2code.pages.main.SearchComponent;
import com.luv2code.pages.main.SearchSuggestionComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class GoogleResultPage {

    private WebDriver driver;
    private SearchComponent searchComponent;
    private SearchSuggestionComponent searchSuggestionComponent;
    private NavigationBarComponent navigationBarComponent;
    private ResultStatComponent resultStatComponent;


    public GoogleResultPage(final WebDriver driver) {
        this.driver = driver;
        this.searchComponent = PageFactory.initElements(driver, SearchComponent.class);
        this.searchSuggestionComponent = PageFactory.initElements(driver, SearchSuggestionComponent.class);
        this.navigationBarComponent = PageFactory.initElements(driver, NavigationBarComponent.class);
        this.resultStatComponent = PageFactory.initElements(driver, ResultStatComponent.class);
    }

    public SearchComponent getSearchComponent() {
        return searchComponent;
    }

    public SearchSuggestionComponent getSearchSuggestionComponent() {
        return searchSuggestionComponent;
    }

    public NavigationBarComponent getNavigationBarComponent() {
        return navigationBarComponent;
    }

    public ResultStatComponent getResultStatComponent() {
        return resultStatComponent;
    }
}
