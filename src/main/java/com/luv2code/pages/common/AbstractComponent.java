package com.luv2code.pages.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractComponent {

    protected WebDriverWait wait;

    public AbstractComponent(final WebDriver driver) {
        wait = new WebDriverWait(driver, 20);
        PageFactory.initElements(driver, this);
    }

    public abstract boolean isDisplayed();

}
