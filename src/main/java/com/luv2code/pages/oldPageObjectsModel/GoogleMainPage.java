package com.luv2code.pages.oldPageObjectsModel;

import com.google.common.util.concurrent.Uninterruptibles;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class GoogleMainPage {

    private WebDriver driver;
    private WebDriverWait wait;

    // LOCATORS
    @FindBy(name = "q")
    private WebElement searchTextBox;

    @FindBy(css = "li.sbct")
    private List<WebElement> suggestions;

    @FindBy(id = "hdtb")
    private WebElement bar;

    @FindBy(linkText = "Images")
    private WebElement imagesLink;

    @FindBy(linkText = "News")
    private WebElement newsLink;

    @FindBy(linkText = "Video")
    private WebElement videoLink;

    @FindBy(id = "result-stats")
    private WebElement resultStat;

    public GoogleMainPage(final WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 20);
        PageFactory.initElements(driver, this);
    }

    public void goTo() {
        this.driver.get("https://www.google.com");
    }


    public void enter(String keyword) {
        this.searchTextBox.clear();
        for (char ch : keyword.toCharArray()) {
            Uninterruptibles.sleepUninterruptibly(50, TimeUnit.MILLISECONDS);
            this.searchTextBox.sendKeys(ch + "");
        }
    }

    public void clickItem(int index) {
        this.suggestions.get(index - 1).click();
    }

    private void gotoImages() {
        this.imagesLink.click();
    }

    private void gotoNews() {
        this.newsLink.click();
    }

    private void gotoVideo() {
        this.videoLink.click();
    }

    public void gotoBarLink(String linkName) {
        if (linkName.toLowerCase().equals("Images")) {
            gotoImages();
        } else if (linkName.toLowerCase().equals("News")) {
            gotoNews();
        } else {
            gotoVideo();
        }
    }





    public String getStat() {
        return this.resultStat.getText();
    }


}
